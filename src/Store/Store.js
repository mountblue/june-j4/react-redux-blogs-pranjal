import { applyMiddleware, createStore } from 'redux'
import rootReducer from '../Reducers/rootReducer'
import thunk from 'redux-thunk';
import logger from 'redux-logger'; 

export default () => {
  return createStore(
    rootReducer,
    applyMiddleware(logger, thunk)
  )
}