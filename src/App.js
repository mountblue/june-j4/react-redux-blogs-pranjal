import React, { Component } from 'react';
import { BrowserRouter, Route, Link } from 'react-router-dom';
import Home from './Components/Home';
import Blogs from './Components/Blogs';
import AddBlog from './Components/AddBlog';
import ViewBlog from './Components/ViewBlog';
import EditBlog from './Components/EditBlog';

class App extends Component {
  render() {
    return (
      <div>
        <BrowserRouter>
          <div className="App">
            <div className="links">
              <Link to="/">Home</Link>
              <Link to="/blogs">Blogs</Link>
              <Link to="/addblog">Add Blog</Link>
            </div>
            <hr />
            <Route exact path="/" component={ Home }/>
            <Route path="/blogs" component={ Blogs }/>
            <Route path="/addblog" component={ AddBlog }/>
            <Route path="/blog/:id" component={ ViewBlog }/>
            <Route path="/blog/edit/:id" component={ EditBlog }/>
          </div>
        </BrowserRouter> 
      </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                            
    );
  }
}

export default App;
