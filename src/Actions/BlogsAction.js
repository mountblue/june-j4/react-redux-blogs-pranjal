import { FETCH_BLOGS, ADD_BLOG, VIEW_BLOG, EDIT_BLOG, DELETE_BLOG } from '../Constants/Types';
import axios from 'axios';

export function getAllBlogs() {
  const request = axios.get(`/api/posts`);
  return (dispatch) => {
    request.then(({data}) => {
      dispatch({
        type: 'FETCH_BLOGS',
        payload: data
      })
    });
  }
}

export function addNewBlog(newBlog) {
  return (dispatch) => {
    fetch('/api/posts', {
      method: 'POST', 
      body: JSON.stringify(newBlog),
      headers:{
        'Content-Type': 'application/json'
      }
    }).then(res => res.json())
    .catch(error => console.error('Error:', error))
    .then(response => console.log('Success:', response))
    .then(response => {
      dispatch({
        type: 'ADD_BLOG',
        payload: newBlog
      })
    })
  }
}

export function viewBlog(id) {
  return (dispatch) => {
    fetch('/api/posts/'+ id, {
      method: 'GET'
    }).then(res => res.json())
    .catch(error => console.error('Error:', error))
    .then(response => console.log('Success:', response))
    .then(response => {
      dispatch({
        type: 'VIEW_BLOG',
        payload: id
      })
    })
  }
};

export function editBlog(updatedBlog, id) {
  return (dispatch) => {
    fetch('/api/posts/'+ id, {
      method: 'PUT', 
      body: JSON.stringify(updatedBlog),
      headers:{
        'Content-Type': 'application/json'
      }
    }).then(res => res.json())
    .catch(error => console.error('Error:', error))
    .then(response => console.log('Success:', response))
    .then(response => {
      dispatch({
        type: 'EDIT_BLOG',
        payload: updatedBlog
      })
    })
  }
};

export function deleteBlog(id) {
  return (dispatch) => {
    fetch('/api/posts/'+ id, {
      method: 'DELETE'
    }).then(res => res.json())
    .catch(error => console.error('Error:', error))
    .then(response => console.log('Success:', response))
    .then(response => {
      dispatch({
        type: 'DELETE_BLOG',
        payload: id
      })
    })
  }
};
