import { combineReducers } from 'redux';
import blogsReducer from './blogsReducer';

const rootReducer = combineReducers({
    blogsReducer
});

export default rootReducer;
