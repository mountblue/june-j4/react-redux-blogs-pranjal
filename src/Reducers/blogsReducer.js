import { FETCH_BLOGS, ADD_BLOG, VIEW_BLOG, EDIT_BLOG, DELETE_BLOG } from '../Constants/Types';

let initialState = [];

export default (state = initialState, action) => {
    switch(action.type) {
      case FETCH_BLOGS:
        return [state, ...action.payload]
      case ADD_BLOG:
        return [...state, action.payload]
      case VIEW_BLOG:
        return [state, ...action.payload]
      case EDIT_BLOG:
        return [...state, ...action.payload]
      case DELETE_BLOG:
        return [...state, ...action.payload]
      default:
        return state
    }
  }