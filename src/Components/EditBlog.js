import React, { Component } from 'react';
import { connect } from 'react-redux';
import { editBlog } from '../Actions/BlogsAction';

class EditBlog extends Component {
  updateBlog = (e) => {
    let editedBlog = {
      _id: this.props.match.params.id,
      title: this.refs.title.value,
      description: this.refs.description.value
    }
    this.props.dispatch(editBlog(editedBlog, this.props.match.params.id));
  }
  componentDidMount(){
    this.props.blogs.map(item => {
      if(item._id === this.props.match.params.id){
        this.refs.title.value = item.title;
        this.refs.description.value = item.description;
      }
    })
  }
  render() {
    console.log(this.props.match.params.id);
    console.log(this.props.blogs);
    return (
      <div className="edit-blog">
        {this.props.blogs.map(item => {
          if(item._id === this.props.match.params.id){
            return (
              <div>
                <div>
                  <label>Title : </label> 
                  <input type="text" ref="title" />
                </div>
                <div>
                  <label>Description : </label> 
                  <textarea ref="description" />
                </div>
                <div>
                  <button onClick={this.updateBlog}>Save</button>
                  <button>Cancel</button>
                </div> 
              </div>
            )
          }
        })}
      </div>
    )
  }
}

function mapStateToProps(state){
  return {
    blogs: state.blogsReducer
  }
}

export default connect(mapStateToProps)(EditBlog);