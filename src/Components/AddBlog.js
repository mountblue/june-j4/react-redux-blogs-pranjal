import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { addNewBlog } from '../Actions/BlogsAction';
import uuid from 'uuid';

class AddBlog extends Component {
  addBlog = (e) => {
    let newBlog = {
      _id: uuid.v4(),
      title: this.refs.title.value,
      description: this.refs.description.value
    }
    this.props.dispatch(addNewBlog(newBlog));
    this.refs.title.value = '';
    this.refs.description.value = '';
  }
  render() {
    let input;
    return (
      <div className="add-blog">
        <div>
            <label>Title : </label> 
            <input type="text" ref="title" />
        </div>
        <div>
            <label>Description : </label> 
            <textarea  ref="description" />
        </div>
        <div>
          <button onClick={this.addBlog}>Add</button>
          <button>Cancel</button>
        </div>
      </div>
    )
  }
}

function mapStateToProps(state){
  return {
    blogs: state.blogsReducer
  }
}

export default connect(mapStateToProps)(AddBlog);