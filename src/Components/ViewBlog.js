import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { viewBlog, deleteBlog } from '../Actions/BlogsAction';

class ViewBlog extends Component {
  removeBlog = (id) => {
    this.props.dispatch(deleteBlog(this.props.match.params.id));
  }
  componentDidMount(){
    this.props.dispatch(viewBlog());
  }
  render() {
    return (
      <div className="view-blog">
        {this.props.blogs.map(item => {
          if(item._id === this.props.match.params.id){
          return(
            <div className="blooog">
              <h3>{item.title}</h3>
              <p>{item.description}</p>
              <div>
                <Link to={"/blog/edit/" + item._id}><button>Edit</button></Link>
                <button onClick={this.removeBlog}>Delete</button>
              </div>
            </div>
          )}})}
        
      </div>
    )
  }
}

function mapStateToProps(state){
  return {
    blogs: state.blogsReducer
  }
}

export default connect(mapStateToProps)(ViewBlog);