import React, { Component } from 'react'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';
import { getAllBlogs } from '../Actions/BlogsAction';

class Blogs extends Component {
  componentDidMount(){
    this.props.dispatch(getAllBlogs());
  }
  render() {
    return (
      <div className="blogs">
        {this.props.blogs.map(item => {
          if(item){
          return(
            <div className="blog">
              <Link to={"/blog/" + item._id}><li id={item._id}>{item.title}</li></Link>
            </div>
          )}})}
      </div>
    )
  }
}

function mapStateToProps(state){
  return {
    blogs: state.blogsReducer
  }
}

export default connect(mapStateToProps)(Blogs);