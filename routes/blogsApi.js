const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

const Blogs = require('../models/blogs');

router.get('/', function (req, res, next) {
    res.render('index');
})

router.get('/api/posts', function (req, res, next) {
    Blogs.find({}).then(function (data) {
        res.send(data);
    })
})

router.post("/api/posts", function (req, res, next) {
    Blogs.create(req.body).then(function (data) {
        res.json({ data: "blog saved successfully!" })
    })
})

router.get("/api/posts/:id", function (req, res, next) {
    Blogs.findOne({_id:req.params.id}).then(function (data) {
        res.json(data);
    })
})

router.delete("/api/posts/:id", function (req, res) {
    Blogs.findByIdAndRemove({ _id: req.params.id }).then(function (todo) {
        res.send({ blog: "blog has been deleted" });
    });
})

router.put("/api/posts/:id", function (req, res, next) {
    Blogs.findByIdAndUpdate(req.params.id, req.body).then(function (data) {
        res.json({ data: "blog update successfully" })
    })
})

module.exports = router;