const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const BlogsSchema = new Schema({
    _id:{
        type:String
    },
    title:{
        type:String
    },
    description:{
        type:String
    },
})

Blogs = mongoose.model("blogs",BlogsSchema);
module.exports = Blogs;